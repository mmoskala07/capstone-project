%% Clear workspace
close all; clc; clearvars;

%% Show plots
show_params_plots = 0;

%% Import headers
params
model
controller

%% Launch simulation
simTime = 10;
t = (0:Ts:simTime)';
disp('Simulation has started...');
sim_result = sim("simulation_extended.slx");
disp('Simulation finished');

%% Show results
run controller_plot
run observer_plot

%% Quality index
err = required_position-sim_result.system_result(:,1);
J_t = sum(t.*(err.^2))