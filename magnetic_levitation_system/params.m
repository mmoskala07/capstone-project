%% Model parameters
g_E = 9.81; % Earth gravity [m/s^2]
m_b = 58e-3; % Ball mass [kg]
b_v = 0; % (can be neglected)

% pwm(voltage) = voltage/12
% coil current(coil transductor): coil transtuctor/1.143
% distance(voltage) = voltage*(-2.2034)+27.2603 
% L(z) = 0.04712/z + 0.1066
% R(z) = 2.94/z + 3.97

%% Global parameters
Ts = 0.01; % Sampling sime [s]

%%
% Finding equivalent/net/resultant/overall resistance of the electrical
% circuit comprising the electromagnet winding and other components.
% Measurements of a steady-stade relationship between the duty factor of a
% PWM control signal and an open-circuit output average voltage of a power
% stage and an output average current of the power stage loaded with the
% winding.


% Power stage output voltage should be low-pass filtered (averaged) for
% measurement. The same applies to the power stage output current.
w = 1:-0.05:0; % [0:0.05:1, 1:-0.05:0];
U = [12.19, 11.5805, 10.971, 10.3615, 9.752, 9.1425, 8.533, 7.9235, ...
     7.314, 6.7045, 6.095, 5.4855, 4.876, 4.2665, 3.657, 3.0475, 2.438, ...
     1.8285, 1.219, 0.6095, 0];
I = [2.21, 2.12, 2.03, 1.93, 1.83, 1.72, 1.62, 1.51, 1.40, 1.29, 1.18, ...
    1.07, 0.95, 0.83, 0.72, 0.60, 0.48, 0.35, 0.23, 0.11, 0.00];

R = U./I;
R_n_mean = mean(R(~isnan(R)));

U_of_I_openCircuit = polyfit(I, U, 1);
R_n_regr = U_of_I_openCircuit(1);

if show_params_plots == 1
    figure('Name', 'Net resistance characteristic')
    subplot(2,2,1);
    plot(w,U,'-*');
    grid on;
    xlabel('PWM duty factor');
    ylabel('current/voltage transducer, V');

    subplot(2,2,2);
    plot(w,I,'-*');
    grid on;
    xlabel('PWM duty factor');
    ylabel('coil current, A');

    subplot(2,2,3);
    plot(I,U,'-*',I,polyval(U_of_I_openCircuit ,I),'-');
    grid on;
    xlabel('coil current, A');
    ylabel('open circuit voltage, V');
    legend('measured','linear regression');

    subplot(2,2,4);
    plot(w,R,'-*', w([1,end]), [1,1]*R_n_mean, w([1,end]), [1,1]*R_n_regr);
    grid on;
    xlabel('PWM duty factor');
    ylabel('resistance, \Omega');
    legend('measured','mean','regression');
end
R_n = (R_n_mean + R_n_regr)/2;
%clear w U I R U_of_I_openCircuit R_n_mean R_n_regr;
% Temperature resistance coefficient of copper:
% alpha = 0.00393





%% power supply characteristic
w = 1:-0.05:0;
U = [12.19, 11.5805, 10.971, 10.3615, 9.752, 9.1425, 8.533, 7.9235, ...
    7.314, 6.7045, 6.095, 5.4855, 4.876, 4.2665, 3.657, 3.0475, 2.438, ...
    1.8285, 1.219, 0.6095, 0];
if show_params_plots == 1
    figure();
    plot(w,U,'-*');
    xlabel('normlised control signal (PWM duty factor)');
    ylabel('open-circuit power-stage output voltage, V');
    pc = polyfit(w,U,1);
    E_ps = pc(1);
    hold on;
    grid on;
    plot(w,polyval(pc,w), 'r-');
end 
clear w U pc;

%% current/voltage transducer  characteristic
w = 1:-0.05:0;
U = [1.93, 1.85, 1.75, 1.65, 1.55, 1.47, 1.39, 1.30, 1.21, 1.12, 1.03, ...
    0.93, 0.83, 0.73, 0.63, 0.53, 0.42, 0.31, 0.21, 0.10, 0.015];
I = [2.25, 2.14, 2.03, 1.93, 1.82, 1.72, 1.61, 1.51, 1.40, 1.29, 1.19, ...
    1.07, 0.95, 0.84, 0.72, 0.60, 0.48, 0.36, 0.24, 0.11, 0.00];

pc = polyfit(I,U,1);
R_m = pc(1); %transresistance

if show_params_plots == 1
    figure();
    plot(I,U,'-*',I,polyval(pc,I),'-');
    grid on;
    xlabel('coil current, A');
    ylabel('current transducer output voltage, V');
    legend('measured', 'regression');

    pc = polyfit(U,I,1);
    G_m = pc(1);
    figure();
    plot(U,I,'-*',U,polyval(pc,U),'-');
    grid on;
    xlabel('coil current, A');
    ylabel('output voltage of the current transducer, V');
    legend('measured', 'regression');
end


%% ball position sensor characteristic

z = 1e-3*(0:0.7:21);
U = [10, 10, 10, 10, 9.98, 9.945, 9.88, 9.795, 9.645, 9.45, 9.195, ...
    8.895, 8.545, 8.18, 7.81, 7.43, 7.06, 6.7, 6.355, 6.035, 5.74, ...
    5.48, 5.25, 5.065, 4.92, 4.82, 4.76, 4.72, 4.71, 4.71, 4.71];

if show_params_plots == 1
    figure();

    title('Ball position sensor characteristic')

    plot(z, U, '-*');
    grid on;
    xlabel('Distance from winidng, $mm$', 'Interpreter', 'latex')
    ylabel('Sensor output voltage, $V$', 'Interpreter', 'latex')
end 

%% 
% Mesaurements of the relationship between the ball-core distance and the
% inductance of the electromagnet winding.
% Measurement conditions (settings of the electronic RCL "bridge"
% instrument):
% f = 
% U = 
% series equivalent circuit
% We should have use decay method to measure/estimate/identify inductance
% of the electromagnet solenoid rather than an RCL bridge. But this method
% is time-consuming and difficult to automate. It would also required firm
% immobilization of the ball as a time-varying and relatively strong
% atraction force would act on it during the test causing it to move and to
% disturb measurement results.

% clear pc
% Approximation versus interpolation.
% Runge's phenomenon (interpolation).
% Numerical rounding error (noise).
% Problem conditioning.
% Centering and scaling input data.
% realmin, realmin('single'), realmax, eps, eps(1), eps(5), eps(0),
% eps[sing]
% Overfitting if too many parameter. Model reproduces measurement errors
% instead of filtering them out and smothing the relationship.
z = 1e-3*(0:0.7:21);
L = 1e-3*[137, 130.6, 126.5, 123.6, 121.4, 119.3, 117.8, 116.5, 115.4, ...
    114.4, 113.5, 112.7, 112, 111.5, 110.8, 110.4, 109.9, 109.5, 109.2, ...
    108.8, 108.5, 108.2, 108, 107.8, 107.6, 107.5, 107.2, 107.1, 106.9, ...
    106.8, 106.7];
R = [7.06, 5.89, 5.44, 5.15, 4.98, 4.81, 4.72, 4.6, 4.55, 4.48, 4.4, ...
    4.35, 4.34, 4.31, 4.27, 4.25, 4.21, 4.2, 4.19, 4.16, 4.14, 4.13, ...
    4.14, 4.09, 4.12, 4.12, 4.09, 4.09, 4.09, 4.06, 4.07];

z = z(6:24);
L = L(6:24);
R = R(6:24);

[xData, yData] = prepareCurveData( z, L );

% Set up fittype.
ft = fittype( 'exp2' );

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft);

z_fitted = z;
L_fitted = feval(fitresult, z_fitted);
[L_dot_fitted, L_double_dot_fitted] = differentiate(fitresult, z_fitted);

if show_params_plots == 1
    figure();
    subplot(2,2,1);
    plot(z,L,'b-*',z_fitted,L_fitted,'r-');
    grid on;
    xlabel('ball-core distance, $m$', 'Interpreter', 'latex');
    ylabel('coil inductance, $H$', 'Interpreter', 'latex');
    legend('Measured', 'Fitted')
    subplot(2,2,3)
    plot(z,R,'-*');
    grid on;
    xlabel('ball-core distance, $m$', 'Interpreter', 'latex');
    ylabel('coil series equivalent resistance, $\Omega$', 'Interpreter', 'latex');
    subplot(2,2,2);
    plot(z_fitted, L_dot_fitted, 'r-');
    grid on;
    xlabel('ball-core distance, $m$', 'Interpreter', 'latex');
    ylabel('approximation of $\frac{dL}{dz}$, $\frac{H}{m}$', 'Interpreter', 'latex');

    subplot(2,2,4)
    plot(z_fitted, L_double_dot_fitted, 'm-');
    grid on;
    xlabel('ball-core distance, $m$', 'Interpreter', 'latex');
    ylabel('approximation of $\frac{d^2L}{dz^2}, \frac{H^2}{m^2}$', 'Interpreter', 'latex');
end

ind.a = fitresult.a;
ind.b = -fitresult.b;
ind.c = 0;
ind.d = fitresult.c;
ind.e = -fitresult.d;
