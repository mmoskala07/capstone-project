%% LQR controller weights
w_control_signal = 0.1;

w_position = 1000;
w_velocity = 9000;
w_current = 0;
%% LQR controller weights matrices

% Matrix with weights for states x
Q = diag([w_position, w_velocity, w_current]);

% Matrix with weights for control u
R = w_control_signal;


%% LQR controller gain
K_LQR = lqrd(A,B,Q,R,Ts)

%% LQI controller
% Matrix with weights for states x
w_position_integral = 700000;
Qi = diag([w_position, w_velocity, w_current, w_position_integral]);
% Matrix with weights for control u
Ri = R;
% State spaces matrices
Ai = A;
Ai(4,4) = 0;
Ai(4,1) = 1;
Bi = [B; 0];

K_LQI = lqrd(Ai, Bi, Qi, Ri, Ts)