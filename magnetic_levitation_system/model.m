%% System equations after linearization

%% Setpoint (desired, reference, commanded) values


z_ep = 0.010; %m
v_ep = 0; % m/s % ball velocity at the equilibrium/operating point
%% Equilibrium point (operational point) determination
% Analytical method, closed-form solution.
% An equilibrium point correspondint to a given (set, desired, reference)
% ball position can be found analitically by equating RHS-s of state space
% equations to zero.


% ind_a = ind.a(:,1);
% ind_b = ind.b(:,1);
% ind_c = ind.c(:,1);
% ind_d = ind.d(:,1);
% ind_e = ind.e(:,1);
% L_ep = ind_a*exp(ind_b*z_ep) + ind_c + ind_d*exp(-ind_e*z_ep);
% L_d_ep = -ind_a*ind_b*exp(-ind_b*z_ep) - ind_d*ind_e*exp(-ind_e*z_ep);
% L_dd_ep = ind_a*ind_b^2*exp(ind_b*z_ep) + ind_d*ind_e^2*exp(-ind_e*z_ep);

L_ep = feval(fitresult, z_ep);
[L_d_ep, L_dd_ep] = differentiate(fitresult, z_ep);

i_ep = sqrt(-2*m_b*g_E/L_d_ep);

u_ep = R_n * i_ep;
% w_ep = u_ep/E_ps
%% Continous state matrices

% That values of A and B matrices we have to receive
% A1 = [0, 0.0010, 0;
%      1.54, 0, -0.0147;
%      0.0, 0.0077, -0.0484];
% A1 = A1 * 1000
% B1 = [0;0;9.0365];

A = [0, 1, 0
    (1/(2*m_b))*L_dd_ep*(i_ep^2), -b_v/m_b, 1/m_b*L_d_ep*i_ep
    (1/L_ep*L_d_ep^2 - 1/L_ep^2*L_dd_ep^2)*v_ep*i_ep, ...
    -1/L_ep*L_d_ep*i_ep, -1/L_ep*L_d_ep*v_ep - R_n/L_ep]

B = [0; 0; 1/L_ep]

C = [1 0 0;
     0 0 1];
 
D = [0; 0];

%% Initial state vector
init_position = 5*z_ep;
init_velocity = 0;
init_current = 0;
init_state = [init_position;
              init_velocity;
              init_current];

%% Required state vector
required_position = z_ep; % m
required_velocity = 0;
required_current = 0;
required_state = [required_position;
                  required_velocity;
                  required_current];


%% Discrete state matrices

ss_discrete = c2d(ss(A, B, C, D), Ts);
[Ad, Bd, Cd, Dd] = ssdata(ss_discrete);


