%% Plot result of Luenberg observer
figure('NumberTitle', 'on', 'Name', 'Luenberger observer', 'units','normalized','outerposition',[0 0 1 1]);
subplot(3, 1, 1);
plot(t, sim_result.luenberger(:,1)); hold on; 
plot(t, sim_result.measurement(:,1)); hold on; 
grid on;
legend("Luenberg observer", "Raw data");
xlabel('Time [s]');
ylabel('Position [m]');

subplot(3, 1, 2);
plot(t, sim_result.luenberger(:,2)); hold on;
grid on;
legend("Luenberg observer");
xlabel('Time [s]');
ylabel('Velocity [A]');

subplot(3, 1, 3);
plot(t, sim_result.luenberger(:,3)); hold on;
plot(t, sim_result.measurement(:,2)); hold on;
grid on;
legend("Luenberg observer", "Raw data");
xlabel('Time [s]');
ylabel('Current [A]');