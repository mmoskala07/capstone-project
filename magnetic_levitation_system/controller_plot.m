%% Plot controller results
figure('NumberTitle', 'on', 'Name', 'Controller', 'units','normalized','outerposition',[0 0 1 1]);
subplot(3, 1, 1);
plot(t, sim_result.system_result(:,1)); hold on; grid on;
ylabel('position [m]');
xlabel('Time [s]');

subplot(3, 1, 2);
plot(t, sim_result.error(:,1)); hold on; grid on;
ylabel('error [m]');
xlabel('Time [s]');

subplot(3, 1, 3);
plot(t, sim_result.steering_signal(:,1)); hold on; grid on;
ylabel('Steering signal');
xlabel('Time [s]');