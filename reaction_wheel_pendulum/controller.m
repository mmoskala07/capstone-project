%% LQR controller weights
w_control_signal = 5;

if is_required_position_lower == 1
    w_pendulum_position = 1000000;
    w_pendulum_velocity = 100;
    w_flywheel_velocity = 10;
else
    w_pendulum_position = 100;
    w_pendulum_velocity = 1000;
    w_flywheel_velocity = 10;
end
%% LQR controller weights matrices

% Matrix with weights for states x
Q = diag([w_pendulum_position, w_pendulum_velocity, w_flywheel_velocity]);

% Matrix with weights for control u
R = [w_control_signal];

%% LQR controller gain
if is_required_position_lower == 1
    K_LQR = lqr(A_lower,B,Q,R);
else
    K_LQR = lqr(A_upper,B,Q,R);
end