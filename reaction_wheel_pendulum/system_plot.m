%% Plot system results
figure('NumberTitle', 'on', 'Name', 'Model', 'units','normalized','outerposition',[0 0 1 1]);
subplot(4, 1, 1);
plot(t, sim_result.system_result(:,1)); hold on; grid on;
ylabel('pendulum [rad]');
xlabel('Time [s]');

subplot(4, 1, 2);
plot(t, sim_result.system_result(:,2)); hold on; grid on;
ylabel('pendulum [rad/s]');
xlabel('Time [s]');

subplot(4, 1, 3);
plot(t, sim_result.system_result(:,3)); hold on; grid on;
ylabel('flywheel [rad/s]');
xlabel('Time [s]');

subplot(4, 1, 4);
plot(t, sim_result.steering_signal(:,1)); hold on; grid on;
ylabel('steering signal [V]');
xlabel('Time [s]');