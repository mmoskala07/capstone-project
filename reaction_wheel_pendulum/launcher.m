%% Clear workspace
close all; clc; clearvars;

%% Global settings
Ts = 0.01; % Sampling sime [s]
max_steering_signal = 24; % Volts [V]

Swing_up_gain = max_steering_signal/4;

is_required_position_lower = 0;
%% Import headers
model
controller

%% Launch simulation
simTime = 30;
t = (0:Ts:simTime)';
disp('Simulation has started...');
% sim_result = sim("linear_state_space_model.slx");
sim_result = sim("non_linear_model.slx");
disp('Simulation finished');

%% Show results
run system_plot