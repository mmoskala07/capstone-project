%% Settings
show_params_plots = false;

%% DC motor doc
% http://www.buehler-motor.pl/download/DC%20Motor_31%20x%2075_1.13.021.6XX.pdf

rated_voltage = 12; % V
rated_power = 11; % W
rated_torque = 3.2e-2; % N·m
rated_speed = (2*pi/60)*3200; % rad/s
rated_current = 1.30; % A
no_load_speed = (2*pi/60)*4300; % rad/s
no_load_current = 0.10; % A
stall_torque = 12e-2; % N·m
stall_current = 4.8; % A
maximum_output_power = 15; % Wx
maximum_constant_torque = 1.9e-2; % N·m
weight = 235e-3; % kg
rotor_inertia = 33e-9; % kg·m²
terminal_resistance = 2.4; % Ω
inductance = 2.3e-3; % H
mechanical_time_constant = 12e-3; % s
electrical_time_constant = 1.0e-3; % s
speed_regulation_constant = (2*pi/60/1e-2)*350; % (rad/s)/(N·m)
torque_constant = 2.7e-2; % N·m/A
thermal_resistance = 13; % K/W
thermal_time_constant = (60)*11; % s
max_axial_play = 0.01e-3; % m

%% MgL identification (mass and centerf of mass position)

m = [94e-3, 56e-3, 161e-3];
r = [180e-3, 307e-3, 103e-3];
g = 9.81; % gravity or Earth

MgL = 0.16587265000000; % mean(m.*r*g)

%% J_theta, u_theta (moment of inertia of the rod, friction coeficient of the rod)

% select experiment
exp1 = load('expData\eksperyment3_male_whaniecia.mat');
exp2 = load('expData\eksperyment4_90st_whaniecia.mat');

% select time range for period calcuation by choosing startpoint and
% endpoint (mouseclick, important only horizontal axis)
if show_params_plots == 1
    hF = figure;
    plot(exp2.StateData.signals(2).values)
    title('Select startpoint and endpoint for period calcuation')
    ylabel('rod position [rad]')
    xlabel('sample')
end

%figure
%plot(exp1.StateData.time, exp1.StateData.signals(2).values)
%hold on

% min and max points from male_wahniecia
t_pos_max = [10.95, 13.38, 15.80];
pos_max = [0.2479, 0.1505, 0.0562];
t_pos_min = [12.17, 14.61]; 
pos_min = [-0.2014, -0.1056];

% Time periods calculated based on measurements
T1 = t_pos_max(2) - t_pos_max(1);
T2 = t_pos_max(3) - t_pos_max(2);
T3 = t_pos_min(2) - t_pos_min(1);
% Mean (average) time period
T_mean = (T1 + T2 + T3)/3;

J_theta = (T_mean^2*MgL)/(4*pi^2)

%change of time range to start from 0
ex2_values = exp2.StateData.signals(2).values(1274:4686);
ex2_time = 0:0.01:34.12;

%local max
if show_params_plots == 1
    TF = islocalmax(ex2_values);
    figure
    plot(ex2_time,ex2_values,ex2_time(TF),ex2_values(TF),'r*')
    ylabel('rod position [rad]');
    xlabel('time[s]');
end
max_x = [0 2.77 5.46 8.08 10.66 13.2 15.72 18.21 20.69 23.16 25.6 ]';
max_y = [1.45173 1.25664 1.08731 0.93775 0.798593 0.670417 0.551037 0.438251 0.332066 0.230594 0.134774].';

%exponential approximation for local maxima
if show_params_plots == 1
    fitt = fittype('1.45173*exp(-a*x)');
    f = fit(max_x,max_y,fitt);
    hold on;
    plot(f);
end

wd = 2*pi/(max_x(2)-max_x(1));
dzeta = 0;
for i=1:(size(max_y,1)-1)
    dzeta = dzeta + log(max_y(i)/max_y(i+1))/(sqrt(4*pi.^2+(log(max_y(i)/max_y(i+1)).^2)));
end
dzeta = dzeta/size(max_y,1);
%dzeta^2 ~= 0 so wd approximetly equal to wn

u_theta = 2*dzeta*MgL/wd
%% J_phi
flywheel_disc_diameter = 109.5e-3; % m
flywheel_disc_thickness = 6.2e-3; % m
pa38_aluminum_alloy_density = 2700; % kg/m^2
pa6_aluminum_alloy_density = 2790; % % kg/m^2
flywheel_disc_density = 0.5*(pa38_aluminum_alloy_density + pa6_aluminum_alloy_density);
flywheel_disc_volume = flywheel_disc_thickness*pi*(flywheel_disc_diameter/2)^2;
flywheel_disc_weight = flywheel_disc_density*flywheel_disc_volume;
flywheel_disc_moment_of_inertia = 0.5*flywheel_disc_weight*(flywheel_disc_diameter/2)^2;
% A boss/hub is made of brass.
flywheel_boss_diameter = 22.2e-3;
flywheel_boss_length = 11e-3;
flywheel_boss_density = (8400 + 8730)/2; % kg/m³
flywheel_boss_volume = flywheel_boss_length*pi*(flywheel_boss_diameter/2)^2;
flywheel_boss_weight = flywheel_boss_density*flywheel_boss_volume;
flywheel_boss_moment_of_inertia = 0.5*flywheel_boss_weight*(flywheel_boss_diameter/2)^2;

exp3 = load('expData\eksperyment11_zablokowane_ramie_identyfikacja-kola.mat');

if show_params_plots == 1
    figure
    subplot(2,1,1)
    plot(exp3.StateData.time(100:end), exp3.StateData.signals(6).values(100:end))
    ylabel('flywheel velocity [rad/s]')
    xlabel('time[s]')
    subplot(2,1,2)
    plot(exp3.StateData.time(100:end), exp3.StateData.signals(1).values(100:end))
    ylim([0 1])
    ylabel('DC control singal [PWM]')
    xlabel('time[s]')
end

flywheel_moment_of_inertia = flywheel_disc_moment_of_inertia + flywheel_boss_moment_of_inertia;
J_phi = flywheel_moment_of_inertia;
%% u_phi (flywheel friction coef)
exp4 = load('expData\eksperyment5_ctrl_0.5_wolnyWybieg.mat');

if show_params_plots == 1
    figure
    plot(exp4.StateData.time(100:end), exp4.StateData.signals(6).values(100:end))
    ylabel('flywheel velocity [rad/s]')
    xlabel('time[s]')
end

discFriction = 1e-5;
u_phi = discFriction;

%% R (overall resistance), PWM to Voltage coef
R_net = 2.9063;
PWM2V = 11.3;

%% Validation
exp5 = load('expData\eksperyment10_impuls_weryfikacja.mat');

if show_params_plots == 1
    figure
    subplot(3,1,1)
    plot(exp5.StateData.time(5:end), exp5.StateData.signals(2).values(5:end))
    ylabel('pend pos [rad]')
    xlabel('time[s]')
    subplot(3,1,2)
    plot(exp5.StateData.time(5:end), exp5.StateData.signals(6).values(5:end))
    ylabel('flywheel velocity [rad/s]')
    xlabel('time[s]')
    subplot(3,1,3)
    plot(exp5.StateData.time(5:end), exp5.StateData.signals(1).values(5:end))
    ylim([-.5 .5])
    ylabel('DC control singal [PWM]')
    xlabel('time[s]')
end

expValidate.time = exp5.StateData.time(5:end);
expValidate.signals.values = exp5.StateData.signals(1).values(5:end);

