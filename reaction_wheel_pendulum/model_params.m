%% Import dependencies
reactionWheelPendulum_identifcation_forStudents

%% To be done

k_t = 0.027000000000000; % torque constant
k_e = 0.027000000000000; % torque constant

MgL = 0.16587265000000; % mean(m.*r*g)

J_theta = 0.024810047373249; % (T_osc^2*MgL)/(4*pi^2)
u_theta = 0.005022712797301;

u_phi = 1e-5; % disc friction
J_phi = 2.424562587877840e-04; % sum of flywheel_disc_moment_of_inertia and flywheel_boss_moment_of_inertia

R = 2.9063; % overall resistance
