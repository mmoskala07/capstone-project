%% Import modules
reactionWheelPendulum_identifcation_forStudents
model

%% Init state for model
init_pendulum_position = pi;
init_pendulum_velocity = 0;
init_flywheel_velocity = 0;

%% Data from real plant
time = exp5.StateData.time;
input_signal = exp5.StateData.signals(1).values * PWM2V;
pendulum_position_real = exp5.StateData.signals(2).values;

%% Data from simulated plant
input_sim = [time input_signal];
disp('Simulation has started...');
sim_result = sim("model_validation_sim.slx");
disp('Simulation finished');

%% Cost function - quality index
% We are subsctracting init_pendulum_position (pi), because real
% measurement data considers pendulum with theta equals 0 in downright
% position
pendulum_position_sim = sim_result.system_result(:,1) - init_pendulum_position;


%% Cost function
J_indicator = sum((pendulum_position_real-pendulum_position_sim).^2)

%% Plot results
subplot(2,1,1);
plot(time, pendulum_position_real); grid on; hold on;
plot(time, pendulum_position_sim); hold on;
ylabel("Pendulum position");
legend('Real model', 'Simulated model');
subplot(2,1,2);
plot(time, (pendulum_position_real-pendulum_position_sim).^2);
ylabel("Squared error");