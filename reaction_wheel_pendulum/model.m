%% Import dependencies
model_params

%% Initial state
if is_required_position_lower == 1
    init_pendulum_position = pi/12;
else
    init_pendulum_position = pi;
end

init_pendulum_velocity = 0;
init_flywheel_velocity = 0;

init_state = [init_pendulum_position;
              init_pendulum_velocity;
              init_flywheel_velocity];

%% Required state
if is_required_position_lower == 1
    required_state = [pi;
                      0;
                      0];
else
    required_state = [0;
                      0;
                      0];
end
%% Continous state matrices
A_upper = [0, 1, 0;
           MgL/J_theta, -u_theta/J_theta, (k_t*k_e+u_phi*R)/(J_theta*R);
           0, 0, -(k_t*k_e-u_phi*R)/(J_phi*R)];

A_lower = [0, 1, 0;
           -MgL/J_theta, -u_theta/J_theta, (k_t*k_e+u_phi*R)/(J_theta*R);
           0, 0, -(k_t*k_e-u_phi*R)/(J_phi*R)];

B = [0; 
    -k_t/(J_theta*R); 
    k_t/(J_phi*R)];

C = [1 0 0;
     0 1 0;
     0 0 1];

D = [0; 0; 0];